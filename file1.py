# reboot instance 
import boto3

client = boto3.client('ec2',region_name='us-east-1')

instance = "i-002d767b9dc2dfa33"

response = client.reboot_instances(
    InstanceIds=[
        instance,
    ],
    DryRun=False
)
print(response)