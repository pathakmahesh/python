# reboot instance 
import boto3

client = boto3.client('ec2',region_name='us-east-1')

instance1 = "i-002d767b9dc2dfa33"
instance2 = "i-065db70063e2030e3"

def reboot(instance):
        response = client.reboot_instances(InstanceIds=[instance,],DryRun=False)
        print ("Reboot response is: {}".format(response))
        print ("Instance ID: {} is rebooted".format(instance))

reboot(instance1)

reboot(instance2)