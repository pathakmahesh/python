# reboot instance 
# list method taking all instances in list format , alway best practice to check into the pythin interpeter what type of data is it ?
# type(instance) --> will gives the list,dist, string etc  
import boto3

client = boto3.client('ec2',region_name='us-east-1')

instance = ["i-002d767b9dc2dfa33","i-065db70063e2030e3"]

def reboot(instance):
        response = client.reboot_instances(InstanceIds=[instance,],DryRun=False)
        print ("Reboot response is: {}".format(response))
        print ("Instance ID: {} is rebooted".format(instance))
        print("\n")

reboot(instance[0])

reboot(instance[1])