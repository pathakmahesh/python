# reboot instance 
# list method taking all instances in list format , alway best practice to check into the pythin interpeter what type of data is it ?
# type(instance) --> will gives the list,dist, string etc  
import boto3
# importing ec2 method
client = boto3.client('ec2',region_name='us-east-1')

instance = {"instance1":"i-002d767b9dc2dfa33","instance2":"i-065db70063e2030e3"} # single or double quote does not matter while defining the dict
# defination or function block
def reboot(instance):
        response = client.reboot_instances(InstanceIds=[instance,],DryRun=False)
        responsecode=response['ResponseMetadata']['HTTPStatusCode']
        print ("Reboot response status is: {}".format(responsecode))
        print ("Instance ID: {} is rebooted".format(instance))
        print("\n")

reboot(instance['instance1'])

reboot(instance['instance2'])