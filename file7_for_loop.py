# type(instance) --> will gives the list,dist, string etc  
import boto3
# importing ec2 method
client = boto3.client('ec2',region_name='us-east-1')

instance = ["i-002d767b9dc2dfa33","i-065db70063e2030e3"] # single or double quote does not matter while defining the dict
# defination or function block

def reboot(instance):
    for inst in instance:
        response = client.reboot_instances(InstanceIds=[inst,],DryRun=False)
        responsecode=response['ResponseMetadata']['HTTPStatusCode']
        if int(responsecode) == 200:
            print ("Instance ID: {} is rebooted".format(inst))
            print("\n")
        else:
            print("instance failed to reboot".format(inst))

reboot(instance)


